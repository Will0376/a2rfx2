package ru.will0376.a2rfx2;

import com.rm5248.serial.NoSuchPortException;
import com.rm5248.serial.NotASerialPortException;
import com.rm5248.serial.SerialPort;
import com.rm5248.serial.SerialPortBuilder;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SerialConnector {
	public static OsCheck.OSType type = OsCheck.getOperatingSystemType();
	public static SerialConnector instance;
	private static boolean but10 = false;
	private static boolean but11 = false;
	public SerialPort port;

	public static SerialPort checkPortWithPrefix(String prefix) {
		for (int i = 0; i < 20; i++) {
			try {
				return new SerialPortBuilder().setBaudRate(SerialPort.BaudRate.B9600).setPort(prefix + i).build();
			} catch (NoSuchPortException | NotASerialPortException e) {
				Controller.instance.print("skipped " + prefix + i);
			} catch (IOException e) {
				System.err.println("An IOException occured");
			}
		}
		return null;
	}

	public static void checkButton(String line) throws Exception {
		if (line.equals("10up")) {
			but10 = true;
			VolumeControl.setSystemVolume(-2f);
		}

		if (line.equals("10down")) but10 = false;

		if (line.equals("11up")) {
			but11 = true;
			VolumeControl.setSystemVolume(2f);
		}

		if (line.equals("11down")) but11 = false;

		if (line.equals("12up") && but10) {
			new Robot().keyPress(KeyEvent.VK_SPACE);
			Controller.instance.print("Space key");
			Thread.sleep(10);
			new Robot().keyRelease(KeyEvent.VK_SPACE);
		}

		if (line.equals("13up") && but10) {
			new Robot().keyPress(KeyEvent.VK_ESCAPE);
			Controller.instance.print("ESC key");
			Thread.sleep(10);
			new Robot().keyRelease(KeyEvent.VK_ESCAPE);
		}

		if (but10 && but11) VolumeControl.muteUn();

		if (line.equals("12up")) {
			new Robot().keyPress(KeyEvent.getExtendedKeyCodeForChar(Controller.instance.buttonNameOne.toCharArray()[0]));
			Controller.instance.plusOne();
		}
		if (line.equals("12down")) {
			new Robot().keyRelease(KeyEvent.getExtendedKeyCodeForChar(Controller.instance.buttonNameOne.toCharArray()[0]));
			Controller.instance.downPlusOne();
		}

		if (line.equals("13up")) {
			new Robot().keyPress(KeyEvent.getExtendedKeyCodeForChar(Controller.instance.buttonNameTwo.toCharArray()[0]));
			Controller.instance.plusTwo();
		}

		if (line.equals("13down")) {
			new Robot().keyRelease(KeyEvent.getExtendedKeyCodeForChar(Controller.instance.buttonNameTwo.toCharArray()[0]));
			Controller.instance.downPlusTwo();
		}
	}

	public void init() {
		instance = this;
		if (type == OsCheck.OSType.Windows) {
			port = checkPortWithPrefix("COM");
		} else {
			port = checkPortWithPrefix("/dev/ttyUSB");
		}

		if (port == null) {
			Controller.println("port == null");
			Controller.instance.stop();
		} else {
			Controller.println("Found port: " + port.getPortName());
			Controller.instance.started = true;
			try {
				InputStream inputStream = port.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
				String line;
				Controller.println("Starting while");
				while ((line = reader.readLine()) != null) {
					Controller.instance.print(line);
					checkButton(line);
				}
				inputStream.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				Controller.println("Stopping while");
				Controller.instance.stop();
			}
		}
	}

	public void close() {
		if (port != null && !port.isClosed()) port.close();
	}
}
