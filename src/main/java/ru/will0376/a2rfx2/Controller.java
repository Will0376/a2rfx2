package ru.will0376.a2rfx2;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {
	public static Controller instance;
	public static Thread th;
	public boolean started = false;
	public String buttonNameOne = "f";
	public String buttonNameTwo = "g";
	@FXML
	TextArea output;
	@FXML
	TextField buttonOne;
	@FXML
	TextField buttonTwo;
	@FXML
	Text buttonOneCounter;
	@FXML
	Text buttonTwoCounter;
	@FXML
	Text buttonOneCounterDown;
	@FXML
	Text buttonTwoCounterDown;
	@FXML
	Text startedText;

	public static void println(String in) {
		instance.print(in);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		instance = this;
		buttonOneCounter.setText("0");
		buttonTwoCounter.setText("0");
		buttonOneCounterDown.setText("0");
		buttonTwoCounterDown.setText("0");
		buttonOne.setText(buttonNameOne);
		buttonTwo.setText(buttonNameTwo);
		startedText.setText("false");

	}

	public void start() {
		SerialConnector main = new SerialConnector();
		if (!started) {
			th = new Thread(main::init);
			th.start();
			print("Started!");
			startedText.setText("true");
		} else {
			stop();
		}
	}

	public void setButtonOne() {
		if (buttonOne.getText().isEmpty()) {
			print("Input button name in field");
			return;
		}
		buttonNameOne = buttonOne.getText().trim();
		print("Set ButtonOne to: " + buttonNameOne);
	}

	public void setButtonTwo() {
		if (buttonTwo.getText().isEmpty()) {
			print("Input button name in field");
			return;
		}
		buttonNameTwo = buttonTwo.getText().trim();
		print("Set ButtonTwo to: " + buttonNameTwo);
	}

	public void print(String text) {
		cleanOut();
		System.out.println(text);
		output.appendText(text + System.lineSeparator());
	}

	public void plusOne() {
		int i = Integer.parseInt(buttonOneCounter.getText());
		buttonOneCounter.setText(String.valueOf(i + 1));
	}

	public void plusTwo() {
		int i = Integer.parseInt(buttonTwoCounter.getText());
		buttonTwoCounter.setText(String.valueOf(i + 1));
	}

	public void downPlusOne() {
		int i = Integer.parseInt(buttonOneCounterDown.getText());
		buttonOneCounterDown.setText(String.valueOf(i + 1));
	}

	public void downPlusTwo() {
		int i = Integer.parseInt(buttonTwoCounterDown.getText());
		buttonTwoCounterDown.setText(String.valueOf(i + 1));
	}

	private void cleanOut() {
		if (output.getText().length() >= 500) output.clear();
	}

	public void stop() {
		started = false;
		startedText.setText("false");
		SerialConnector.instance.close();
		th.stop();
		print("Stopped!");
	}
}
