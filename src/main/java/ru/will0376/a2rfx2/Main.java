package ru.will0376.a2rfx2;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	public static Scene scene;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(ClassLoader.getSystemResource("main.fxml"));
		Parent root = fxmlLoader.load();
		primaryStage.setTitle("A2RFX2");
		primaryStage.setScene(new Scene(root, 498, 276));
		scene = primaryStage.getScene();
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	@Override
	public void stop() throws Exception {
		Controller.instance.stop();
		super.stop();
	}
}

