package ru.will0376.a2rfx2;

import java.io.*;
import java.net.URL;

public class LibCopy {

	public File getLibFile() {
		try {
			File file = null;
			String resource = "nircmd.exe";
			URL res = ClassLoader.getSystemResource(resource);
			if (res.getProtocol().equals("jar")) {
				try {
					InputStream input = ClassLoader.getSystemResource(resource).openStream();
					file = File.createTempFile("nircmd.exe", ".tmp");
					OutputStream out = new FileOutputStream(file);
					int read;
					byte[] bytes = new byte[1024];

					while ((read = input.read(bytes)) != -1) {
						out.write(bytes, 0, read);
					}
					out.close();
					file.deleteOnExit();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			} else {
				file = new File(res.getFile());
			}

			if (file != null && !file.exists()) {
				throw new RuntimeException("Error: File " + file + " not found!");
			}
			return file;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
