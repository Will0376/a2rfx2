package ru.will0376.a2rfx2;

import java.io.File;
import java.io.IOException;

public class VolumeControl {
	public static boolean mute = false;
	private static final File nircmdFilePath = new LibCopy().getLibFile();

	public static void setSystemVolume(float volume) {
		double endVolume = 655.35 * volume;
		Runtime rt = Runtime.getRuntime();
		try {
			Process pr = rt.exec(nircmdFilePath + " changesysvolume  " + endVolume);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void muteUn() {

		Runtime rt = Runtime.getRuntime();
		try {
			String doit = "1";
			if (mute) doit = "0";
			Process pr = rt.exec(nircmdFilePath + " mutesysvolume " + doit);
			mute = !mute;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}